# Angular Express Bootstrap Kinvey Seed

This is a seed project to start your site with Kinvey. If you dont use Kinvey, then just replace the api.js with what ever backend service you like!

## Whats included
AngularJS
NodeJS w/ Express
Angular Strap
Kinvey
MixPanel

## How to use

Clone the repository,

if you use Kinvey, change `api.js` lines 8 and 9 to match your kinvey options
if you use MixPanel, in `public/js/app.js` to your key

run `npm install` to grab the dependencies, and start hacking!



### Running the app

Runs like a typical express app:

    `node app.js`

### Heroku
The Procfile is already set up, you just need to add the repo.
so just type  `git remote add heroku git@heroku.com:test-angularjs-new.git`


