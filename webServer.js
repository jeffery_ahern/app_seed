/**
 * Module dependencies.
 */

var express = require('express');
var api = require('./api');
var PORT = process.env.PORT || 3000;

var app = module.exports = express();

//noinspection JSValidateTypes
app.configure(function () {
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.static(__dirname + '/public'));
	app.use(app.router);
});

app.configure('development', function () {
	app.use(express.errorHandler({ dumpExceptions:true, showStack:true }));
});

app.configure('production', function () {
	app.use(express.errorHandler());
});


// JSON API

app.get('/api/name', api.name);
app.get('/api/boo', api.boo);
app.get('/api/ping', api.ping);

// Start server

app.listen(PORT, function () {
	console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});
