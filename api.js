/*
 * Serve JSON to our AngularJS client
 */

var Kinvey = require('kinvey');

//noinspection JSValidateTypes
Kinvey.init({
	appKey: 'kid_PVbAOMC_s5',
	appSecret: '1bd943a6180146e3a144fcf678100615'
});

exports.name = function (req, res) {
	res.json({
		name:'Bob'
	});
};

exports.boo = function (req, res) {

	// Fetch all books.
	var bookCollection = new Kinvey.Collection('auctions');

	bookCollection.fetch({
		success: function(auctions) {
			res.json({
				auctions: auctions
			});
		},
		error: function(error) {
			res.json({
				error: error
			});
		}

	});
};

exports.ping = function (req, res) {

	Kinvey.ping({
		success: function(response) {
			res.json({
				msg: 'Kinvey Ping Success. Kinvey Service is alive',
				version: response.version,
				response: response.kinvey
			});
		},
		error: function(error) {
			res.json({
				msg: 'Kinvey Ping Failed.',
				error: error.description
			});
			console.log('Kinvey Ping Failed. Response: ' + error.description);
		}
	});

	// Fetch all books.
	var bookCollection = new Kinvey.Collection('auctions');

	bookCollection.fetch({
		success: function(auctions) {
			res.json({
				auctions: auctions
			});
		},
		error: function(error) {
			res.json({
				error: error
			});
		}

	});
};

