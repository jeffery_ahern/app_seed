'use strict';


// Declare app level module which depends on filters, and services
var myApp = angular.module('myApp', ['ngResource', 'myApp.controllers', 'myApp.filters', 'myApp.services', 'myApp.directives', '$strap.directives']);

myApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {templateUrl: 'pages/home.html', controller: 'MyCtrl1'});
    $routeProvider.when('/about', {templateUrl: 'pages/about.html', controller: 'MyCtrl2'});
    $routeProvider.otherwise({redirectTo: '/'});
  }]);

myApp.constant('MIX_PANEL_KEY', '42c7c8af3d01fc9dcd2a1dc16fe9d236');
