'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
	.controller('MyCtrl2', [function() {

  }]);

function MyCtrl1($scope, MixPanel) {
	MixPanel.track("Event", {"gender": "male", "age": 13}, function() { console.log('track call succeeded') });
};